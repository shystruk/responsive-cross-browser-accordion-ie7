/**
 * Created by v.stokolosa on 3/20/15.
 */
/*jslint plusplus: true, browser:true, node: true*/

'use strict';

//cross-browser function (IE)
function getElementsByClassName(node, classname) {
    var data = [],
        reqExp = new RegExp('(^| )' + classname + '( |$)'),
        els = node.getElementsByTagName('*'),
        i, j;

    for (i = 0, j = els.length; i < j; i++)
        if (reqExp.test(els[i].className)) {
            data.push(els[i]);
        }
    return data;
}

function Accordion() {
    //data
    this.tab = getElementsByClassName(document.body, 'module-tab');
    this.firstTab = getElementsByClassName(document.body, 'module-tab-first');
    this.ulTabs = getElementsByClassName(document.body, 'module-tabs');
    this.tabText = getElementsByClassName(document.body, 'module-tab-text');
    this.close = getElementsByClassName(document.body, 'module-close');
    this.tabsMobile = getElementsByClassName(document.body, 'module-tab-mobile');
    this.menu = getElementsByClassName(document.body, 'module-icon');
    this.menuTabs = getElementsByClassName(document.body, 'module-tabs-mobile');
    this.menuIcon = getElementsByClassName(document.body, 'module-icon-image');
    this.block1 = document.getElementById('block-1');
    this.block2 = document.getElementById('block-2');
    this.block3 = document.getElementById('block-3');
    this.tabTextLen = this.tabText.length;
    this.closeLen = this.close.length;

    //height & width for tab
    this.ulTabsHeight = this.ulTabs[0].offsetHeight;
    this.ulTabs[0].style.height = this.ulTabsHeight + 'px';
    this.tab[0].style.width = (this.firstTab[0].offsetWidth) + 'px';
}

var app = new Accordion();

Accordion.prototype.clickHandler = function (event) {
    event = event || window.event;

    var target = event.target || event.srcElement,
        dataTab = target.getAttribute('data-tab'),
        div = target.parentNode,
        parent = div.parentNode,
        actionParent,
        actionData,
        actionDiv,
        i, j;

    switch (dataTab) {
        case 'tab-1':
            actionsClick(app.block1, app.block2, app.block3, 'tab-1');
            break;
        case 'tab-2':
            actionsClick(app.block2, app.block1, app.block3, 'tab-2');
            break;
        case 'tab-3':
            actionsClick(app.block3, app.block1, app.block2, 'tab-3');
            break;
    }

    function actionsClick(ownBlock, hideBlock1, hideBlock2, tab) {
        ownBlock.className = "module-content-block active";

        for (i = 0; i < app.tab.length; i++) {
            app.tab[i].className = 'module-tab';
            app.tab[i].style.width = '';
        }

        for (j = 0; j < app.tabsMobile.length; j++) {
            app.tabsMobile[j].className = 'module-tab-mobile';
        }

        parent.style.width = (div.offsetWidth) + 'px';
        parent.className = parent.className + ' active';

        hideBlock1 !== null ? hideBlock1.className = 'module-content-block' : '';
        hideBlock2 !== null ? hideBlock2.className = 'module-content-block' : '';

        for (i = 0; i < app.tabTextLen; i++) {
            actionDiv = app.tabText[i].parentNode;
            actionParent = actionDiv.parentNode;
            actionData = app.tabText[i].getAttribute('data-tab');
            if (actionData === tab) {
                actionParent.className = actionParent.className + ' active';
            }
        }
    }
};


Accordion.prototype.closeHandler = function (event) {
    event = event || window.event;

    var target = event.target || event.srcElement,
        dataTab = target.getAttribute('data-tab'),
        tabs,
        lastTab,
        actionParent,
        actionData,
        actionDiv, i;

    switch (dataTab) {
        case 'tab-1':
            actionsClose(app.block1, 'tab-1');
            break;
        case 'tab-2':
            actionsClose(app.block2, 'tab-2');
            break;
        case 'tab-3':
            actionsClose(app.block3, 'tab-3');
            break;
    }

    function actionsClose(removeBlc, tab) {
        removeBlc.parentNode.removeChild(removeBlc);

        for (i = 0; i < app.tabTextLen; i++) {
            actionDiv = app.tabText[i].parentNode;
            actionParent = actionDiv.parentNode;
            actionData = app.tabText[i].getAttribute('data-tab');
            if (actionData === tab) {
                actionParent.parentNode.removeChild(actionParent);
            }
        }

        tabs = getElementsByClassName(document.body, 'module-tab');

        if (tabs.length === 1) {
            lastTab = getElementsByClassName(document.body, 'module-tab-first');
            tabs[0].style.position = 'static';
            lastTab[0].style.top = app.ulTabsHeight + 'px';
        }
    }
};

Accordion.prototype.menuHandler = function () {
    if (app.menuTabs[0].style.display === 'block') {
        app.menuTabs[0].style.display = 'none';
        app.menuIcon[0].src = 'image/menu1.png';
    } else {
        app.menuTabs[0].style.display = 'block';
        app.menuTabs[0].style.top = app.ulTabs[0].offsetHeight + 'px';
        app.menuIcon[0].src = 'image/menu.png';
    }
};


if (document.addEventListener) {
    document.addEventListener("DOMContentLoaded", function () {
        for (var i = 0; i < app.tabTextLen; i++) {
            app.tabText[i].addEventListener('click', app.clickHandler, false);
        }
        for (var j = 0; j < app.closeLen; j++) {
            app.close[j].addEventListener('click', app.closeHandler, false);
        }
        app.menu[0].addEventListener('click', app.menuHandler, false);
    }, false);
}
else if (document.attachEvent) {
    document.attachEvent("onreadystatechange", function () {
        for (var i = 0; i < app.tabTextLen; i++) {
            app.tabText[i].attachEvent('onclick', app.clickHandler);
        }
        for (var j = 0; j < app.closeLen; j++) {
            app.close[j].attachEvent('onclick', app.closeHandler);
        }
    });
}
